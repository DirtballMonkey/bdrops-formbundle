<?php

namespace Bdrops\FormBundle\Form;

use RevisionTen\Forms\Model\FormRead;
use Doctrine\ORM\EntityRepository;
use KK\PageBundle\Form\ComponentType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DisplayComponentType extends ComponentType
{
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return 'Form';
    }

    /**
     * {@inheritdoc}
     */
    public function getIcon()
    {
        return '<i class="fa fa-file-o"></i>';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('form', EntityType::class, array(
            'label'    => 'Form',
            'required' => false,
            'class' => FormRead::class,
            'query_builder' => function (EntityRepository $repository) {
                return $repository->createQueryBuilder('f')
                    ->where('f.deleted IS NULL OR f.deleted = 0')
                    ->orderBy('f.title', 'ASC');
            },
            'choice_label' => 'title',
            'attr' => array(
                'placeholder' => 'Form',
            ),
        ));

        $builder->add('submissionLimit', NumberType::class, array(
            'label' => 'Maximale Anzahl an Einsendungen',
            'required' => false,
        ));

        $builder->add('submissionLimitMessage', TextareaType::class, array(
            'label' => '"Maximale Anzahl erreicht" Hinweistext',
            'required' => false,
            'attr' => [
                'placeholder' => 'Dieser Text erscheint wenn das Limit erreicht wurde.',
            ],
        ));

        $builder->add('counterMessage', TextareaType::class, array(
            'label' => 'Anzahl Hinweistext',
            'required' => false,
            'attr' => [
                'placeholder' => 'Sie können die Platzhalter %countTaken%, %countOpen% und %limit% verwenden um die Anzahl an Einsendungen und das Limit anzuzeigen.',
            ],
        ));

        $builder->add('data', DataType::class, array(
            'label'    => 'Vorauswahl',
            'required' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bdrops\FormBundle\Entity\DisplayComponent',
            'authorization_checker' => false,
            'roles' => false,
            'pagebundle_parameters' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'formbundle_formdisplaycomponent';
    }

}
