<?php

namespace Bdrops\FormBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use RevisionTen\CQRS\Services\AggregateFactory;
use RevisionTen\CQRS\Services\CommandBus;
use RevisionTen\CQRS\Services\MessageBus;
use RevisionTen\Forms\Controller\FormController;
use RevisionTen\Forms\Model\FormRead;
use RevisionTen\Forms\Model\FormSubmission;
use RevisionTen\Forms\Services\FormService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class DisplayController extends AbstractController
{
    protected $formController;

    public function __construct(FormController $formController)
    {
        $this->formController = $formController;
    }

    public function renderFormAction(
        RequestStack $requestStack,
        string $formUuid,
        string $template = null,
        array $defaultData,
        ?int $submissionLimit = null,
        ?string $submissionLimitMessage = null,
        ?string $counterMessage = null
    ): Response
    {
        $response = $this->formController->renderFormAction($requestStack, $formUuid, $template, $defaultData);

        $count = 0;
        if (!empty($submissionLimit)) {
            $entityManager = $this->getDoctrine();
            /**
             * @var FormRead|null $formRead
             */
            $formRead = $entityManager->getRepository(FormRead::class)->findOneBy([
                'uuid' => $formUuid,
            ]);
            if (null !== $formRead) {
                $formSubmissions = $entityManager->getRepository(FormSubmission::class)->findBy([
                    'form' => $formRead,
                ]);
                $count = $formSubmissions !== null ? count($formSubmissions) : 0;
            }
        }

        if (!empty($submissionLimit) && !empty($counterMessage) && ($count < $submissionLimit)) {
            $content = $response->getContent();
            $counterMessage = str_replace('%countTaken%', $count, $counterMessage);
            $counterMessage = str_replace('%countOpen%', ($submissionLimit - $count), $counterMessage);
            $counterMessage = str_replace('%limit%', $submissionLimit, $counterMessage);
            $content = $counterMessage.$content;
            $response->setContent($content);
        }

        if (!empty($submissionLimit) && !empty($submissionLimitMessage) && $count >= $submissionLimit) {
            $content = $response->getContent();
            $content = $submissionLimitMessage.$content;
            $response->setContent($content);
        }

        return $response;
    }
}
