#FormBundle

---

##Anforderungen

PHP 7.1+


##Installation

Folgende Composer Angaben sind zu empfehlen (PHP Version durch die Version austauschen die auf dem Production-Server läuft):

```JSON
"prefer-stable": true,
"minimum-stability": "dev",
"config": {
    "platform": {
        "php": "7.1.12"
    }
},
```



Das Bundle zur composer.json hinzufügen:
```JSON
"require": {
    // ...
    "bdrops/formbundle": "@dev"
},
```

[...]

```JSON
"repositories": [{
     "type": "vcs",
     "url": "https://bitbucket.org/DirtballMonkey/bdrops-forms.git"
 }, {
     "type": "vcs",
     "url": "https://bitbucket.org/DirtballMonkey/bdrops-cqrs.git"
 }, {
     "type": "vcs",
     "url": "https://bitbucket.org/DirtballMonkey/bdrops-formbundle.git"
 }],

```


`composer update` ausführen.

Die Bundles zum AppKernel hinzufügen (vor dem PageBundle):
```PHP
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        
        new \Bdrops\Forms\FormsBundle(),
        new \Bdrops\CQRS\CqrsBundle(),
        new \Bdrops\FormBundle\FormBundle(),
            
        // ...
    );
}
```


Das Component Template in der components.html.twig einbinden:

```TWIG
...
{% use '@Form/DisplayComponent.html.twig' %}
...
```

Die Component zur PageBundle Konfiguration hinzufügen:

```YAML
page:
    types:
        - 'Bdrops\FormBundle\Form\DisplayComponentType'

```

Die Routes zum routing.yml hinzufügen (vor dem PageBundle):

```YAML
formsbundle:
    resource: "@FormsBundle/Resources/config/routes.yaml"
    prefix:   /
```

(Optional) Das Bundle Konfigurieren:

```YAML
forms:
    load_easyadmin_config: true
    item_types:
        #GroupItem:
        #    class: Bdrops\Forms\Form\Items\GroupItem
        #    icon: 'fa-object-group'
        TextItem:
            class: Bdrops\Forms\Form\Items\TextItem
            icon: 'fa-comment'
        TextAreaItem:
            class: Bdrops\Forms\Form\Items\TextAreaItem
            icon: 'fa-book'
        EmailItem:
            class: Bdrops\Forms\Form\Items\EmailItem
            icon: 'fa-at'
        CheckboxItem:
            class: Bdrops\Forms\Form\Items\CheckboxItem
            icon: 'fa-check-square'
        ChoiceItem:
            class: Bdrops\Forms\Form\Items\ChoiceItem
            icon: 'fa-list-ul'
        EntityItem:
            class: Bdrops\Forms\Form\Items\EntityItem
            icon: 'fa-database'
        MarkupItem:
            class: Bdrops\Forms\Form\Items\MarkupItem
            icon: 'fa-code'
        SubmitItem:
            class: Bdrops\Forms\Form\Items\SubmitItem
            icon: 'fa-paper-plane'

```
