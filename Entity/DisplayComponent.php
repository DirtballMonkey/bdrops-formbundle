<?php

namespace Bdrops\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RevisionTen\Forms\Model\FormRead;
use KK\PageBundle\Entity\Component;

/**
 * DisplayComponent
 *
 * @ORM\Table(name="display_components")
 * @ORM\Entity()
 */
class DisplayComponent extends Component
{
    /**
     * @var string
     */
    protected $discr = 'DisplayComponent';

    /**
     * @var FormRead
     * @ORM\ManyToOne(targetEntity="\RevisionTen\Forms\Model\FormRead")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @ORM\Cache("NONSTRICT_READ_WRITE", region=CACHEREGION)
     */
    private $form;

    /**
     * @var array
     * @ORM\Column(type="text", nullable=true)
     */
    private $data;

    /**
     * @var integer|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $submissionLimit;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $submissionLimitMessage;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $counterMessage;

    /**
     * @return mixed
     */
    public function getForm(): ?FormRead
    {
        return $this->form;
    }

    /**
     * @param FormRead|null $form
     * @return DisplayComponent
     */
    public function setForm(FormRead $form = null): DisplayComponent
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return json_decode($this->data, true);
    }

    /**
     * @param array|null $data
     * @return DisplayComponent
     */
    public function setData(array $data = null): DisplayComponent
    {
        $this->data = json_encode($data);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubmissionLimit()
    {
        return $this->submissionLimit;
    }

    /**
     * @param int|null $submissionLimit
     * @return DisplayComponent
     */
    public function setSubmissionLimit($submissionLimit)
    {
        $this->submissionLimit = $submissionLimit;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubmissionLimitMessage()
    {
        return $this->submissionLimitMessage;
    }

    /**
     * @param string|null $submissionLimitMessage
     * @return DisplayComponent
     */
    public function setSubmissionLimitMessage($submissionLimitMessage)
    {
        $this->submissionLimitMessage = $submissionLimitMessage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterMessage()
    {
        return $this->counterMessage;
    }

    /**
     * @param string|null $counterMessage
     * @return DisplayComponent
     */
    public function setCounterMessage($counterMessage)
    {
        $this->counterMessage = $counterMessage;

        return $this;
    }
}
